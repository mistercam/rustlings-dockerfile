# Rustlings Dockerfile

I created this Dockerfile to quickly spin up a container with the latest version of [Rustlings](https://github.com/rust-lang/rustlings) installed and ready to run. The rustlings files are accessible on your local machine as well (in the directory where you created the container,) which allows you to edit them within an IDE or with an editor outside of the container. At the same time, rustlings -- running in the container -- will test your changes each time you save a file.

To get started, copy the Dockerfile into a directory of your choice.

Then from within a terminal:

1. Build the image: `docker build -t rustlings-image --no-cache .`
2. Create the container: `docker create --name rustlings -v $(pwd):/app/rustlings -it rustlings-image`
3. Start the container: `docker start -i rustlings`

At this point you'll be connected to the container in your terminal.

Run `rustlings` to begin, and follow the instructions.