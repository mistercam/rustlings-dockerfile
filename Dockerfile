# A Dockerfile for setting up Rustlings
#
# Build the image:
#   docker build -t rustlings-image --no-cache .
#
# Create the container:
#   docker create --name rustlings -v $(pwd):/app/rustlings -it rustlings-image
#
# Start the container:
#   docker start -i rustlings
#
# Stop the container:
#   docker stop rustlings
#
# Within the running container, execute 'rustlings' and follow the
# instructions.
#
# You can either edit the exercise files within the container,
# or locally through your preferred IDE or editor.

# Use the official rust image with the latest version
FROM rust:latest

# Install gcc
RUN apt-get update && apt-get install -y gcc

# Set /app as the working directory
WORKDIR /app

# Install Clippy
RUN rustup component add clippy

RUN mkdir -p rustlings-tmp 
RUN cd rustlings-tmp && cargo install rustlings
RUN cd rustlings-tmp && rustlings init

RUN mkdir -p /tmp

# Create a script that copies everything in /app/rustlings-tmp
# into /app/rustlings, if /app/rustlings-tmp exists. After
# executing, /app/rustlings-tmp will be deleted. Thus, this
# action will only ever be performed once when starting the
# container for the first time.
RUN echo '#!/bin/bash\nif [ -d /app/rustlings-tmp ]; then\n    cp -r /app/rustlings-tmp/. /app\n    rm -Rf /app/rustlings-tmp\nfi\nexec "$@"' > /tmp/startup.sh && chmod +x /tmp/startup.sh

# Switch to rustlings directory on startup
RUN echo "cd /app/rustlings" >> /root/.bashrc

# Run the script, which executes 'bash' as the final step
CMD ["/tmp/startup.sh", "bash"]
